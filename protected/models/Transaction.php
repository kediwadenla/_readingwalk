<?php

/**
 * This is the model class for table "tbl_transaction".
 *
 * The followings are the available columns in table 'tbl_transaction':
 * @property integer $transaction_id
 * @property integer $user_id
 * @property integer $total
 * @property string $payment
 * @property string $comment
 * @property string $insert_date
 *
 * The followings are the available model relations:
 * @property User $user
 * @property TransactionDetil[] $transactionDetils
 */
class Transaction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, total, payment, comment, insert_date', 'required'),
			array('user_id, total', 'numerical', 'integerOnly'=>true),
			array('payment', 'length', 'max'=>40),
			array('comment', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('transaction_id, user_id, total, payment, comment, insert_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'transactionDetils' => array(self::HAS_MANY, 'TransactionDetil', 'transaction_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'transaction_id' => 'Transaction',
			'user_id' => 'User',
			'total' => 'Total',
			'payment' => 'Payment',
			'comment' => 'Comment',
			'insert_date' => 'Insert Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('transaction_id',$this->transaction_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('total',$this->total);
		$criteria->compare('payment',$this->payment,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('insert_date',$this->insert_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
