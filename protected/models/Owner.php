<?php

/**
 * This is the model class for table "tbl_owner".
 *
 * The followings are the available columns in table 'tbl_owner':
 * @property integer $owner_id
 * @property string $owner_name
 * @property string $owner_address
 * @property string $owner_email
 * @property integer $owner_phone
 *
 * The followings are the available model relations:
 * @property Book[] $books
 */
class Owner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_owner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('owner_name, owner_address, owner_email, owner_phone', 'required'),
			array('owner_phone', 'numerical', 'integerOnly'=>true),
			array('owner_name', 'length', 'max'=>40),
			array('owner_address', 'length', 'max'=>128),
			array('owner_email', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('owner_id, owner_name, owner_address, owner_email, owner_phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'books' => array(self::HAS_MANY, 'Book', 'owner_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'owner_id' => 'Owner',
			'owner_name' => 'Owner Name',
			'owner_address' => 'Owner Address',
			'owner_email' => 'Owner Email',
			'owner_phone' => 'Owner Phone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('owner_id',$this->owner_id);
		$criteria->compare('owner_name',$this->owner_name,true);
		$criteria->compare('owner_address',$this->owner_address,true);
		$criteria->compare('owner_email',$this->owner_email,true);
		$criteria->compare('owner_phone',$this->owner_phone);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Owner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
