<?php

/**
 * This is the model class for table "tbl_paket".
 *
 * The followings are the available columns in table 'tbl_paket':
 * @property integer $paket_id
 * @property string $paket_name
 * @property integer $max_book
 * @property string $valid_date
 *
 * The followings are the available model relations:
 * @property UserPaket[] $userPakets
 */
class Paket extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_paket';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paket_name, max_book, valid_date', 'required'),
			array('max_book', 'numerical', 'integerOnly'=>true),
			array('paket_name', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('paket_id, paket_name, max_book, valid_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userPakets' => array(self::HAS_MANY, 'UserPaket', 'paket_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'paket_id' => 'Paket',
			'paket_name' => 'Paket Name',
			'max_book' => 'Max Book',
			'valid_date' => 'Valid Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('paket_id',$this->paket_id);
		$criteria->compare('paket_name',$this->paket_name,true);
		$criteria->compare('max_book',$this->max_book);
		$criteria->compare('valid_date',$this->valid_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
