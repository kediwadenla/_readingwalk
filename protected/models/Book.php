<?php

/**
 * This is the model class for table "tbl_book".
 *
 * The followings are the available columns in table 'tbl_book':
 * @property integer $book_id
 * @property string $title
 * @property string $author
 * @property integer $page
 * @property string $language
 * @property string $category
 * @property string $image
 * @property string $genre
 * @property string $description
 * @property integer $price
 * @property integer $owner_id
 * @property integer $use
 *
 * The followings are the available model relations:
 * @property Owner $owner
 * @property TransactionDetil[] $transactionDetils
 */
class Book extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_book';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, author, page, language, category, image, genre, description, price, owner_id, use', 'required'),
			array('image', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true),
			array('page, price, owner_id, use', 'numerical', 'integerOnly'=>true),
			array('title, author, language, category, genre', 'length', 'max'=>50),
			array('image, description', 'length', 'max'=>1000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('book_id, title, author, page, language, category, image, genre, description, price, owner_id, use', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'owner' => array(self::BELONGS_TO, 'Owner', 'owner_id'),
			'transactionDetils' => array(self::HAS_MANY, 'TransactionDetil', 'book_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'book_id' => 'Book',
			'title' => 'Title',
			'author' => 'Author',
			'page' => 'Page',
			'language' => 'Language',
			'category' => 'Category',
			'image' => 'Image',
			'genre' => 'Genre',
			'description' => 'Description',
			'price' => 'Price',
			'owner_id' => 'Owner',
			'use' => 'Use',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('book_id',$this->book_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('page',$this->page);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('genre',$this->genre,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('owner_id',$this->owner_id);
		$criteria->compare('use',$this->use);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Book the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
