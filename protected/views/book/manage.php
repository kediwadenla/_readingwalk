<?php
	/* Display all books */
?>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <center>Are you sure want to delete this book?</center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="<?php echo Yii::app()->createUrl('book/delete',array('id'=>1));?>" class="btn btn-default" >Yes</a>
            </div>
        </div>
    </div>
</div>

<table class="table">
	<tr>
		<th>ID</th>
		<th>Judul</th>
		<th>Author</th>
		<th>Page</th>
		<th>Language</th>
		<th>Category</th>
		<th>Genre</th>
		<th>Price</th>
		<th>Owner</th>
		<th>Action</th>
	</tr>
	<?php if(isset($models) && $models !== NULL) : ?>
		<?php foreach ($models as $model) : ?>
			<tr>
				<td><?php echo $model->book_id;?></td>
				<td><?php echo $model->title;?></td>
				<td><?php echo $model->author;?></td>
				<td><?php echo $model->page;?></td>
				<td><?php echo $model->language;?></td>
				<td><?php echo $model->category;?></td>
				<td><?php echo $model->genre;?></td>
				<td><?php echo $model->price;?></td>
				<td><?php echo $model->owner_id?></td>
				<td>
					<a href="<?php echo $this-> createUrl ('book/update', array('id'=>$model->book_id));?>"><i class="fa fa-edit fa-lg"></i></a>
                                        <a href="#" data-toggle="modal" data-target="#deleteModal" data-bookId="<?php echo $model->book_id;?>"><i class="fa fa-times fa-lg"></i></a>
				</td>
			</tr>
		<?php endforeach;?>
	<?php endif;?>
</table>