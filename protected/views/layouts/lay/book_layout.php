<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>ReadingWalk</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="<?php echo Yii::app()->request->baseUrl;?>/assets/theme/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl;?>/assets/theme/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/assets/theme/css/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/assets/theme/css/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/assets/theme/css/js/scripts.js"></script>
</head>
<body>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="#">ReadingWalk</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<i class="fa fa-cart-plus fa-2x" style="padding-right: 50px; color: white; padding-left: 10px; padding-bottom: 10px"></i>
						</li>
						<li>
							<a href="#" style="padding-right:15px; padding-bottom: 10px">Register</a>
						</li>
						<li>
							<a href="#" style="padding-right:40px; padding-bottom: 10px">LogIn</a>
						</li>
					</ul>
				</div>
				
</nav>
<br><br><br>
<div class="container">
<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-md-2 column" style="margin-top:50px">
					<div class="panel-group" id="panel-794225" style="margin-bottom: 5px">
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-905286"><i class="fa fa-book" style="margin-right:15px;"></i>Book</a>
							</div>
							<div id="panel-element-905286" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('book/create');?>">Add</a></li>
								</div>
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('book/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-298581"><i class="fa fa-database fa-lg" style="margin-right:12px;"></i>Book Owner</a>
							</div>
							<div id="panel-element-298581" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('owner/create');?>">Add</a></li>
								</div>
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('owner/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-797816"><i class="fa fa-user-plus fa-lg" style="margin-right:7px;"></i>User</a>
							</div>
							<div id="panel-element-797816" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('user/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-905284"><i class="fa fa-book" style="margin-right:15px;"></i>Paket</a>
							</div>
							<div id="panel-element-905284" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('paket/create');?>">Add</a></li>
								</div>
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('paket/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-905285"><i class="fa fa-book" style="margin-right:15px;"></i>Transaksi</a>
							</div>
							<div id="panel-element-905285" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('transaction/create');?>">Add</a></li>
								</div>
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('transaction/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 column" style="margin-left:70px">
					<?php echo $content;?>
				</div>
			</div>
			</div>
		</div>
	</div>

<br><br>	
<div class="row clearfix">
		<div class="col-md-12 column" style="background-color:black; opacity:0.9">
			<div class="col-md-4 col-sm-4">
				<center>
					<a href="">Home</a>&nbsp;|&nbsp;
					<a href="http://readingwalk.com/index.php/site/aboutus">About Us</a>&nbsp;|&nbsp;
					<a href="http://readingwalk.com/index.php/site/howtoorder">How to Order</a>&nbsp;|&nbsp;
					<a href="http://readingwalk.com/index.php/site/faq">FAQ</a>
				</center>
			</div>
			<div class="col-md-4 col-sm-4 no-padding">
				<center>© Copyright 2015 - 2020 Ked. All Rights Reserved</center>
			</div>
			<div class="col-md-4 col-sm-4">
				<center>
					<a href="https://www.facebook.com/" target="_blank"><span class="social-box-o"><i class="fa fa-facebook"></i></span> facebook</a>&nbsp;&nbsp;
					<a href="https://twitter.com/" target="_blank"><span class="social-box-o"><i class="fa fa-twitter"></i></span> twitter</a>&nbsp;&nbsp;
					<a href="http://readingwalk.com/#"><span class="social-box-o"><i class="fa fa-google-plus"></i></span> google+</a>
				</center>
			</div>
		</div>
</div>
</body>
</html>

