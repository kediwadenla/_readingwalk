<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/main'); ?>
	<div class="col-md-3">
		<div class="search">
			<form role="search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="search">
						<div class="input-group-btn">
							<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
						</div>
					</div>
			</form>
		</div>
		
		<div class = "span3">
			<ul class="nav">
				<li><a href="<?php echo Yii::app()->createUrl('');?>" class="active" id="itemHome" style="padding-left:2px;">Katalog</a></li>
				<li><a href="<?php echo Yii::app()->createUrl('');?>" class="active" id="itemHome" style="padding-left:2px;">Buku Pilihan Kami</a></li>
				<li><a href="<?php echo Yii::app()->createUrl('');?>" class="active" id="itemHome" style="padding-left:2px;">Konsultasi Buku</a></li>
				<li><a href="<?php echo Yii::app()->createUrl('');?>" class="active" id="itemHome" style="padding-left:2px;">Cara Peminjaman</a></li>
				<li><a href="<?php echo Yii::app()->createUrl('');?>" class="active" id="itemHome" style="padding-left:2px;">Kontak RW</a></li>
				<li><a href="<?php echo Yii::app()->createUrl('');?>" class="active" id="itemHome" style="padding-left:2px;">About Us</a></li>
			</ul>
		</div>
	</div>
	
	<div class="col-md-9">
		<?php echo $content; ?>
	</div>
<?php $this->endContent(); ?>