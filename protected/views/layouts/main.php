<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>ReadingWalk</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
	
		<link href="<?php echo Yii::app()->request->baseUrl;?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->request->baseUrl;?>/css/style.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/css/font-awesome.min.css">

		<!-- Fav and touch icons -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="img/favicon.png">
  
	</head>

	<body>
		<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<center><h4 class="modal-title"><i class="fa fa-sign-in"></i> Login</h4></center>
					</div>
					<div class="modal-body">
						<form class="form-group login-form" action="<?php echo Yii::app()->createUrl('/site/login')?>" method="POST">
							
								<input class="form-control" type="text" placeholder="Username" name="LoginForm[username]"/>
							<br />
								<input class="form-control" type="password" placeholder="Password" name="LoginForm[password]"/>
							
							<?php if(Yii::app()->user->hasFlash('error')) : ?>
							<div class="alert alert-danger">
								<center><?php echo Yii::app()->user->getFlash('error'); ?></center>
							</div>
							<?php endif;?>
							<br/>
							<center><button type="submit" class="btn btn-primary">Log In</button></center>
						</form>
						<center><a class="forgot" href="#">Lupa Password?</a></center>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<center><h4 class="modal-title"><i class="glyphicon glyphicon-shopping-cart"></i>Cart</h4></center>
					</div>
					<div class="modal-body">
						<?php
							$items = Cart::model()->findAllByAttributes(array('user_id'=>Yii::app()->user->id));
							foreach($items as $item) : 
						?>
						<div class="cart-item">
							<div class="cart-img">
							
							</div>
							<div class="cart-title">
							
							</div>
							<div class="price">
							
							</div>
						</div>
						<?php endforeach; ?>
						<div class="cart-sum">
							
						</div>
					</div>
					<div class="modal-footer">
						<center><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></center>
					</div>
				</div>
			</div>
		</div>
			
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class = "container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="<?php echo Yii::app()->homeUrl;?>">ReadingWalk</a>
				</div>
			
				<?php if(Yii::app()->user->isGuest) :	?>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="<?php echo Yii::app()->createUrl('site/register');?>" style="padding-right:15px; padding-bottom: 10px">Register</a>
						</li>
						<li>
							<a href="#" data-toggle="modal" data-target="#loginModal" style="padding-right:40px; padding-bottom: 10px">Login</a>
						</li>
					</ul>
				</div>
				<?php else : ?>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#" data-toogle="modal" data-target="#cartModal"><i class="fa fa-cart-plus fa-2x" style="padding-right: 50px; color: white; padding-left: 10px; padding-bottom: 10px"></i></a>
						</li>
						<li>
							<a href="<?php echo Yii::app()->createUrl('site/logout');?>" style="padding-right:15px; padding-bottom: 10px">Logout</a>
						</li>
						<li>
							<a href="<?php echo Yii::app()->createUrl('user/profile');?>" style="padding-right:40px; padding-bottom: 10px"><?php echo Yii::app()->user->name;?></a>
						</li>
					</ul>
				</div>	
				<?php endif; ?>
			</div>
		</nav>
		
		<div class="container">
			<div class="logo">
				<h1 class="text-center"><a href="<?php echo Yii::app()->homeUrl;?>">Reading<span>Walk</span> 
				<hr class="onepixel">
				<small>ONLINE DELIVERY BOOK RENTAL</small></a></h1>
			</div>
		
			<div class="carousel slide" id="myCarousel" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-slide-to="0" data-target="#myCarousel" class="active"></li>
					<li data-slide-to="1" data-target="#myCarousel"></li>
					<li data-slide-to="2" data-target="#myCarousel"></li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<img alt="" src="http://lorempixel.com/1600/500/sports/1" />
						<div class="carousel-caption">
							<h4>
								First Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" src="http://lorempixel.com/1600/500/sports/2" />
						<div class="carousel-caption">
							<h4>
								Second Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" src="http://lorempixel.com/1600/500/sports/3" />
						<div class="carousel-caption">
							<h4>
								Third Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
				</div> 
				<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> 
				<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
			
			<?php echo $content; ?>
                   
		</div>
	
		<div class="footer">
                    <div class="container">
			<div class="row">
				<center>
					<a href="<?php echo Yii::app()->homeUrl;?>">Home</a>&nbsp;|&nbsp;
					<a href="<?php echo Yii::app()->createUrl('site/aboutUs');?>">About Us</a>&nbsp;|&nbsp;
					<a href="<?php echo Yii::app()->createUrl('site/howToOrder');?>">How to Order</a>&nbsp;|&nbsp;
					<a href="<?php echo Yii::app()->createUrl('site/faq');?>">FAQ</a>
				</center>	
			</div>
                    
			<div class = "row">
				<center>© Copyright 2015 - 2020 Reading Walk. All Rights Reserved</center>
			</div>
                    </div>
		</div>
		
		<script src="<?php echo Yii::app()->request->baseUrl;?>/css/js/jquery.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl;?>/css/js/bootstrap.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl;?>/css/js/scripts.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl;?>/css/js/readingwalk.js"></script>
	</body>
</html>