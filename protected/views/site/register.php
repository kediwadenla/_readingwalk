<?php $this->beginContent('//layouts/mainReg'); ?>

<div class="reg-form">
	<center>
		<h2>Registrasi</h2>
	</center>
	
	<div class ="form">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'register-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>false,
		)); ?>

			<p class="note">Fields with <span class="required">*</span> are required.</p>
				<div class="form-group">
					<div class="row">
						<?php echo $form->labelEx($userModel,'username', array('class'=> 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->textField($userModel,'username',array('size'=>40,'maxlength'=>50,'class'=>'form-control')); ?>
						</div>
						<?php echo $form->error($userModel,'Username'); ?>
					</div>
				</div>
					
				<div class="form-group">
					<div class="row">
						<?php echo $form->labelEx($userModel,'password',array('class'=> 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->passwordField($userModel,'password',array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
						</div>
						<?php echo $form->error($userModel,'password'); ?>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<?php echo $form->labelEx($userModel,'email',array('class'=> 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->emailField($userModel,'email',array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
						</div>
						<?php echo $form->error($userModel,'email'); ?>
					</div>
				</div>
					
				<div class="form-group">
					<div class="row">
						<?php echo $form->labelEx($customerModel,'name',array('class'=> 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
						<?php echo $form->textField($customerModel,'name',array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
						</div>
						<?php echo $form->error($customerModel,'name'); ?>
					</div>
				</div>
					
				<div class="form-group">
					<div class="row">
						<?php echo $form->labelEx($customerModel,'gender',array('class'=> 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
						<?php echo $form->dropDownList($customerModel,'gender', array('Male'=>'Male','Female'=>'Female'),array('class'=>'form-control', 'empty'=>'-- Select Gender --')); ?>
						</div>
						<?php echo $form->error($customerModel,'gender'); ?>
					</div>
				</div>
					
				<div class="form-group">
					<div class="row">
						<?php echo $form->labelEx($customerModel,'address',array('class'=> 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
						<?php echo $form->textField($customerModel,'address',array('size'=>60,'maxlength'=>256, 'class'=>'form-control')); ?>
						</div>
						<?php echo $form->error($customerModel,'address'); ?>
					</div>
				</div>
					
				<div class="form-group">
					<div class="row">
						<?php echo $form->labelEx($customerModel,'pos_code',array('class'=> 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
						<?php echo $form->textField($customerModel,'pos_code', array('class'=>'form-control')); ?>
						</div>
						<?php echo $form->error($customerModel,'pos_code'); ?>
					</div>
				</div>
					
				<div class="form-group">
					<div class="row">
						<?php echo $form->labelEx($customerModel,'city',array('class'=> 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
						<?php echo $form->textField($customerModel,'city',array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
						</div>
						<?php echo $form->error($customerModel,'city'); ?>
					</div>
				</div>
					
				<div class="form-group">
					<div class="row">
						<?php echo $form->labelEx($customerModel,'phone',array('class'=> 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
						<?php echo $form->textField($customerModel,'phone', array('class'=>'form-control')); ?>
						</div>
						<?php echo $form->error($customerModel,'phone'); ?>
					</div>
				</div>
				
				<div class="form-group">
					<?php echo CHtml::submitButton($userModel->isNewRecord ? 'Create' : 'Save'); ?>
				</div>

		<?php $this->endWidget(); ?>

	</div><!-- form -->
</div>

<?php $this->endContent(); ?>