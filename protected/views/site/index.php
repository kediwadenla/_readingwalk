<?php $this->beginContent('//layouts/column2'); ?>

<?php if(isset($models)) : ?>
<?php foreach($models as $model): ?>
	<div class="col-md-3">
		<div class="thumbnail">
			<img alt="<?php echo $model->title . ' ' . $model->category . ' ' . $model->genre;?>" src="<?php echo Yii::app()->request->baseUrl . $model->image;?>">
			<div class="caption">
				<div class="thumb-title">
					<?php echo $model->title;?>
				</div>
				<div class="thumb-desc">
					Author : <?php echo $model->author;?> Year : <?php echo $model->year;?>Stock : <?php echo $model->stock;?>
				</div>
				<p>
					<a href="#" class="btn btn-primary">Rent</a>
				</p>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<?php endif; ?>

<?php $this->endContent(); ?>
