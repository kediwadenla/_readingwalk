<?php
/* @var $this TransactionController */
/* @var $model Transaction */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'transaction-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<div class="row">
			<?php echo $form->labelEx($model,'user_id',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'user_id',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'user_id'); ?>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
			<?php echo $form->labelEx($model,'total',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'total',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'total'); ?>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
			<?php echo $form->labelEx($model,'payment',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'payment',array('size'=>40,'maxlength'=>40,'class'=>'form-control')); ?>
				<?php echo $form->error($model,'payment'); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="row">
			<?php echo $form->labelEx($model,'comment',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'comment',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
				<?php echo $form->error($model,'comment'); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="row">
			<?php echo $form->labelEx($model,'insert_date',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'insert_date',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'insert_date'); ?>
			</div>
		</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->	