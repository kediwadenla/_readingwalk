<?php
/* @var $this OwnerController */
/* @var $model Owner */

$this->breadcrumbs=array(
	'Owners'=>array('index'),
	$model->owner_id=>array('view','id'=>$model->owner_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Owner', 'url'=>array('index')),
	array('label'=>'Create Owner', 'url'=>array('create')),
	array('label'=>'View Owner', 'url'=>array('view', 'id'=>$model->owner_id)),
	array('label'=>'Manage Owner', 'url'=>array('admin')),
);
?>

<h1>Update Owner <?php echo $model->owner_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>