<?php
/* @var $this PaketController */
/* @var $model Paket */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'paket_id'); ?>
		<?php echo $form->textField($model,'paket_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paket_name'); ?>
		<?php echo $form->textField($model,'paket_name',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'max_book'); ?>
		<?php echo $form->textField($model,'max_book'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valid_date'); ?>
		<?php echo $form->textField($model,'valid_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->