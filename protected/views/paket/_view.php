<?php
/* @var $this PaketController */
/* @var $data Paket */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('paket_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->paket_id), array('view', 'id'=>$data->paket_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paket_name')); ?>:</b>
	<?php echo CHtml::encode($data->paket_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_book')); ?>:</b>
	<?php echo CHtml::encode($data->max_book); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valid_date')); ?>:</b>
	<?php echo CHtml::encode($data->valid_date); ?>
	<br />


</div>