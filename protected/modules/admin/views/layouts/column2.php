<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/mainAdmin'); ?>
	
				<div class="col-md-3 column" style="margin-top:50px">
					<div class="panel-group" id="panel-794225" style="margin-bottom: 5px">
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-905286"><i class="fa fa-book" style="margin-right:15px;"></i>Book</a>
							</div>
							<div id="panel-element-905286" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('book/create');?>">Add</a></li>
								</div>
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('book/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-298581"><i class="fa fa-database fa-lg" style="margin-right:12px;"></i>Book Owner</a>
							</div>
							<div id="panel-element-298581" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('owner/create');?>">Add</a></li>
								</div>
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('owner/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-797816"><i class="fa fa-user-plus fa-lg" style="margin-right:7px;"></i>User</a>
							</div>
							<div id="panel-element-797816" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('user/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-905284"><i class="fa fa-book" style="margin-right:15px;"></i>Paket</a>
							</div>
							<div id="panel-element-905284" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('paket/create');?>">Add</a></li>
								</div>
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('paket/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-794225" href="#panel-element-905285"><i class="fa fa-book" style="margin-right:15px;"></i>Transaksi</a>
							</div>
							<div id="panel-element-905285" class="panel-collapse collapse">
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('transaction/create');?>">Add</a></li>
								</div>
								<div class="panel-body">
									<li><a href="<?php echo $this-> createUrl ('transaction/manage');?>">Manage</a></li>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-9 column" style="margin-left:70px">
					<?php echo $content;?>
				</div>
			</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-9">
		<?php echo $content; ?>
	</div>
<?php $this->endContent(); ?>