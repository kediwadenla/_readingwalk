<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>ReadingWalk</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="<?php echo Yii::app()->request->baseUrl;?>/assets/theme/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl;?>/assets/theme/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" href="<?php echo Yii::app()->request->baseUrl;?>assets/theme/css/js/jquery.min.js"></script>
	<script type="text/javascript" href="<?php echo Yii::app()->request->baseUrl;?>assets/theme/css/js/bootstrap.min.js"></script>
	<script type="text/javascript" href="<?php echo Yii::app()->request->baseUrl;?>assets/theme/css/js/scripts.js"></script>
</head>

<body>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="#">ReadingWalk</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<i class="fa fa-cart-plus fa-2x" style="padding-right: 50px; color: white; padding-left: 10px; padding-bottom: 10px"></i>
						</li>
						<li>
							<a href="#" style="padding-right:15px; padding-bottom: 10px">Register</a>
						</li>
						<li>
							<a href="#" style="padding-right:40px; padding-bottom: 10px">LogIn</a>
						</li>
					</ul>
				</div>
				
</nav>
<br><br><br>

<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="logo">
						<h1 class="text-center"><a href="index.html">Reading<span>Walk</span> 
						<hr class="onepixel">
						<small>ONLINE DELIVERY BOOK RENTAL</small></a></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="container">	
<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="carousel slide" id="carousel-458881">
				<ol class="carousel-indicators">
					<li class="active" data-slide-to="0" data-target="#carousel-458881">
					</li>
					<li data-slide-to="1" data-target="#carousel-458881">
					</li>
					<li data-slide-to="2" data-target="#carousel-458881">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<img alt="" src="http://lorempixel.com/1600/500/sports/1" />
						<div class="carousel-caption">
							<h4>
								First Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" src="http://lorempixel.com/1600/500/sports/2" />
						<div class="carousel-caption">
							<h4>
								Second Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" src="http://lorempixel.com/1600/500/sports/3" />
						<div class="carousel-caption">
							<h4>
								Third Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
				</div> <a class="left carousel-control" href="#carousel-458881" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-458881" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
		</div>
	</div>        
     
	<hr class="onepixel">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-md-2 column">
					<div class="row clearfix">
						<div class="col-md-12 column">
							<form class="navbar-form" role="search" style="padding:0; margin:0 0 10px 0;">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search" name="q">
										<div class="input-group-btn">
											<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
										</div>
									</div>
								</form>
							<div class="btn-group btn-group-vertical btn-group-lg" style="width:165px">
							<button class="btn btn-default" type="button" style="padding-left: 5px; text-align: left; color: #9d0404">Home</button>
							<button class="btn btn-default" type="button" style="padding-left: 5px; text-align: left; color: #9d0404">About Us</button>
							<button class="btn btn-default" type="button" style="padding-left: 5px; text-align: left; color: #9d0404">Kontak RW</button>
							<button class="btn btn-default" type="button" style="padding-left: 5px; text-align: left; color: #9d0404">Katalog</button> 
							<button class="btn btn-default" type="button" style="padding-left: 5px; text-align: left; color: #9d0404">Buku Pilihan Kami</button> 
							<button class="btn btn-default" type="button" style="padding-left: 5px; text-align: left; color: #9d0404">Konsultasi Buku</button> 
							<button class="btn btn-default" type="button" style="padding-left: 5px; text-align: left; color: #9d0404">How to Order</button>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-10 column">
					<div class="row clearfix">
						<div class="col-md-12 column">
							<div class="row">
								<div class="col-md-4">
									<div class="thumbnail">
										<img alt="300x200" src="http://lorempixel.com/600/200/people">
										<div class="caption">
											<h3>
												Thumbnail label
											</h3>
											<p>
												Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
											</p>
											<p>
												<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
											</p>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="thumbnail">
										<img alt="300x200" src="http://lorempixel.com/600/200/city">
										<div class="caption">
											<h3>
												Thumbnail label
											</h3>
											<p>
												Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
											</p>
											<p>
												<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
											</p>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="thumbnail">
										<img alt="300x200" src="http://lorempixel.com/600/200/sports">
										<div class="caption">
											<h3>
												Thumbnail label
											</h3>
											<p>
												Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
											</p>
											<p>
												<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="thumbnail">
										<img alt="300x200" src="http://lorempixel.com/600/200/people">
										<div class="caption">
											<h3>
												Thumbnail label
											</h3>
											<p>
												Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
											</p>
											<p>
												<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
											</p>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="thumbnail">
										<img alt="300x200" src="http://lorempixel.com/600/200/city">
										<div class="caption">
											<h3>
												Thumbnail label
											</h3>
											<p>
												Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
											</p>
											<p>
												<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
											</p>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="thumbnail">
										<img alt="300x200" src="http://lorempixel.com/600/200/sports">
										<div class="caption">
											<h3>
												Thumbnail label
											</h3>
											<p>
												Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
											</p>
											<p>
												<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
<div class="row clearfix">
		<div class="col-md-12 column" style="background-color:black; opacity:0.9;">
			<div class="col-md-4 col-sm-4">
				<center>
					<a href="">Home</a>&nbsp;|&nbsp;
					<a href="http://fndel.com/index.php/site/aboutus">About Us</a>&nbsp;|&nbsp;
					<a href="http://fndel.com/index.php/site/howtoorder">How to Order</a>&nbsp;|&nbsp;
					<a href="http://fndel.com/index.php/site/faq">FAQ</a>
				</center>
			</div>
			<div class="col-md-4 col-sm-4 no-padding">
				<center>© Copyright 2015 - 2020 Ked. All Rights Reserved</center>
			</div>
			<div class="col-md-4 col-sm-4">
				<center>
					<a href="https://www.facebook.com/fndelbandung" target="_blank"><span class="social-box-o"><i class="fa fa-facebook"></i></span> facebook</a>&nbsp;&nbsp;
					<a href="https://twitter.com/fndel_bdg" target="_blank"><span class="social-box-o"><i class="fa fa-twitter"></i></span> twitter</a>&nbsp;&nbsp;
					<a href="http://fndel.com/#"><span class="social-box-o"><i class="fa fa-google-plus"></i></span> google+</a>
				</center>
			</div>
		</div>
</div>
</body>
</html>

