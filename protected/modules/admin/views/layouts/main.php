<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>ReadingWalk</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
	
		<link href="<?php echo Yii::app()->request->baseUrl;?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->request->baseUrl;?>/css/admin.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/css/font-awesome.min.css">

		<!-- Fav and touch icons -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="img/favicon.png">
  
	</head>

	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class = "container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="<?php echo Yii::app()->homeUrl;?>">ReadingWalk</a>
				</div>
			
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="<?php echo Yii::app()->createUrl('site/logout');?>" style="padding-right:15px; padding-bottom: 10px">Logout</a>
						</li>
						<li>
							<a href="#" style="padding-right:40px; padding-bottom: 10px"><?php echo Yii::app()->user->name;?></a>
						</li>
					</ul>
				</div>	
				
			</div>
		</nav>
		
		<div class="container">
			
			
			<?php echo $content; ?>
                   
		</div>
	
		<div class="footer">
                    <div class="container">
			<div class="row">
				<center>
					<a href="<?php echo Yii::app()->homeUrl;?>">Home</a>&nbsp;|&nbsp;
					<a href="<?php echo Yii::app()->createUrl('site/aboutUs');?>">About Us</a>&nbsp;|&nbsp;
					<a href="<?php echo Yii::app()->createUrl('site/howToOrder');?>">How to Order</a>&nbsp;|&nbsp;
					<a href="<?php echo Yii::app()->createUrl('site/faq');?>">FAQ</a>
				</center>	
			</div>
                    
			<div class = "row">
				<center>© Copyright 2015 - 2020 Reading Walk. All Rights Reserved</center>
			</div>
                    </div>
		</div>
		
		<script src="<?php echo Yii::app()->request->baseUrl;?>/css/js/jquery.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl;?>/css/js/bootstrap.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl;?>/css/js/scripts.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl;?>/css/js/readingwalk.js"></script>
	</body>
</html>