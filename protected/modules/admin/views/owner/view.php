<?php
/* @var $this OwnerController */
/* @var $model Owner */

$this->breadcrumbs=array(
	'Owners'=>array('index'),
	$model->owner_id,
);

$this->menu=array(
	array('label'=>'List Owner', 'url'=>array('index')),
	array('label'=>'Create Owner', 'url'=>array('create')),
	array('label'=>'Update Owner', 'url'=>array('update', 'id'=>$model->owner_id)),
	array('label'=>'Delete Owner', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->owner_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Owner', 'url'=>array('admin')),
);
?>

<h1>View Owner #<?php echo $model->owner_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'owner_id',
		'owner_name',
		'owner_address',
		'owner_email',
		'owner_phone',
	),
)); ?>
