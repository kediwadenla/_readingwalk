<?php
/* @var $this OwnerController */
/* @var $data Owner */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('owner_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->owner_id), array('view', 'id'=>$data->owner_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('owner_name')); ?>:</b>
	<?php echo CHtml::encode($data->owner_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('owner_address')); ?>:</b>
	<?php echo CHtml::encode($data->owner_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('owner_email')); ?>:</b>
	<?php echo CHtml::encode($data->owner_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('owner_phone')); ?>:</b>
	<?php echo CHtml::encode($data->owner_phone); ?>
	<br />


</div>