<?php
	/* Display all books */
?>

<table class="table">
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Address</th>
		<th>Email</th>
		<th>Phone Number</th>
		<th>Action</th>
	</tr>
	<?php if(isset($models) && $models !== NULL) : ?>
		<?php foreach ($models as $model) : ?>
			<tr>
				<td><?php echo $model->owner_id;?></td>
				<td><?php echo $model->owner_name;?></td>
				<td><?php echo $model->owner_address;?></td>
				<td><?php echo $model->owner_email;?></td>
				<td><?php echo $model->owner_phone;?></td>
				<td>
					<a href="<?php echo $this-> createUrl ('owner/update', array('id'=>$model->owner_id));?>"><i class="fa fa-edit fa-lg"></i></a>
					<a href="<?php echo $this-> createUrl ('owner/del', array('id'=>$model->owner_id));?>" onclick="return confirm('Are you sure you wish to delete this Record?');"><i class="fa fa-times fa-lg"></i></a>
				</td>
			</tr>
		<?php endforeach;?>
	<?php endif;?>
</table>