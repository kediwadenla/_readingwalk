<?php
/* @var $this OwnerController */
/* @var $model Owner */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'owner-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'Name', array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'owner_name',array('size'=>40,'maxlength'=>40,'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'owner_name'); ?>
			</div>
		</div>
			
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'Address',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'owner_address',array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'owner_address'); ?>
			</div>
		</div>
			
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'Email',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'owner_email', array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'owner_email'); ?>
			</div>
		</div>
			
		<div class="form-group">	
			<div class="row">
				<?php echo $form->labelEx($model,'Phone Number',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->numberField($model,'owner_phone',array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'owner_phone'); ?>
			</div>
		</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->