<?php
/* @var $this PaketController */
/* @var $model Paket */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'paket-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<div class="row">
			<?php echo $form->labelEx($model,'paket_name',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'paket_name',array('size'=>40,'maxlength'=>40,'class'=>'form-control')); ?>
				<?php echo $form->error($model,'paket_name'); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="row">
			<?php echo $form->labelEx($model,'max_book',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'max_book',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'max_book'); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="row">
			<?php echo $form->labelEx($model,'valid_date',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->dateField($model,'valid_date',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'valid_date'); ?>
			</div>
		</div>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->