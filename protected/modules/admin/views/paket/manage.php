<?php
	/* Display all books */
?>

<table class="table">
	<tr>
		<th>ID</th>
		<th>Paket Name</th>
		<th>Max Book</th>
		<th>Valid Date</th>
		<th>Action</th>
	</tr>
	<?php if(isset($models) && $models !== NULL) : ?>
		<?php foreach ($models as $model) : ?>
			<tr>
				<td><?php echo $model->paket_id;?></td>
				<td><?php echo $model->paket_name;?></td>
				<td><?php echo $model->max_book;?></td>
				<td><?php echo $model->valid_date;?></td>
				<td>
					<a href="<?php echo $this-> createUrl ('paket/update', array('id'=>$model->paket_id));?>"><i class="fa fa-edit fa-lg"></i></a>
					<a href="<?php echo $this-> createUrl ('paket/del', array('id'=>$model->paket_id));?>" onclick="return confirm('Are you sure you wish to delete this Record?');"><i class="fa fa-times fa-lg"></i></a>
				</td>
			</tr>
		<?php endforeach;?>
	<?php endif;?>
</table>