<?php
/* @var $this PaketController */
/* @var $model Paket */

$this->breadcrumbs=array(
	'Pakets'=>array('index'),
	$model->paket_id=>array('view','id'=>$model->paket_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Paket', 'url'=>array('index')),
	array('label'=>'Create Paket', 'url'=>array('create')),
	array('label'=>'View Paket', 'url'=>array('view', 'id'=>$model->paket_id)),
	array('label'=>'Manage Paket', 'url'=>array('admin')),
);
?>

<h1>Update Paket <?php echo $model->paket_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>