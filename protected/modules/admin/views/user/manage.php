<?php
	/* Display all books */
?>

<table class="table">
	<tr>
		<th>ID</th>
		<th>User Name</th>
		<th>Password</th>
		<th>Email</th>
		<th>Date</th>
	</tr>
	<?php if(isset($models) && $models !== NULL) : ?>
		<?php foreach ($models as $model) : ?>
			<tr>
				<td><?php echo $model->user_id;?></td>
				<td><?php echo $model->username;?></td>
				<td><?php echo $model->password;?></td>
				<td><?php echo $model->email;?></td>
				<td><?php echo $model->insert_date;?></td>
				<td>
					<a href="<?php echo $this-> createUrl ('user/update', array('id'=>$model->user_id));?>"><i class="fa fa-edit fa-lg"></i></a>
					<a href="<?php echo $this-> createUrl ('user/del', array('id'=>$model->user_id));?>" onclick="return confirm('Are you sure you wish to delete this Record?');"><i class="fa fa-times fa-lg"></i></a>
				</td>
			</tr>
		<?php endforeach;?>
	<?php endif;?>
</table>