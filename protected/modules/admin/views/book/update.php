<?php
/* @var $this BookController */
/* @var $model Book */

$this->breadcrumbs=array(
	'Books'=>array('index'),
	$model->title=>array('view','id'=>$model->book_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Book', 'url'=>array('index')),
	array('label'=>'Create Book', 'url'=>array('create')),
	array('label'=>'View Book', 'url'=>array('view', 'id'=>$model->book_id)),
	array('label'=>'Manage Book', 'url'=>array('admin')),
);
?>

<center><h1>Update Book</h1></center>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>