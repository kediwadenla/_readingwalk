<?php
/* @var $this BookController */
/* @var $model Book */
/* @var $form CActiveForm */
?>


<?php 
    if(Yii::app()->user->hasFlash('error')){
        Yii::app()->clientScript->registerScript('showMassage','$("#messageModal").modal("show");');
    }
?>

<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <center><?php echo Yii::app()->user->getFlash('message');?></center>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div class ="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'book-form',
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<?php echo $form->errorSummary($model); ?>
			
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'title', array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'title',array('size'=>40,'maxlength'=>50,'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'title'); ?>
			</div>
		</div>
			
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'author',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'author',array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'author'); ?>
			</div>
		</div>
			
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'page',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->numberField($model,'page', array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'page'); ?>
			</div>
		</div>
			
		<div class="form-group">	
			<div class="row">
				<?php echo $form->labelEx($model,'language',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'language',array('size'=>40,'maxlength'=>40, 'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'language'); ?>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'category',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'category',array('size'=>40,'maxlength'=>60, 'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'category'); ?>
			</div>
		</div>
	
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'image',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo CHtml::activeFileField($model,'image',array('size'=>60,'maxlength'=>128, 'class'=>'input-file', 'type'=>'file')); ?>
				</div>
				<?php echo $form->error($model,'image'); ?>
			</div>
				<?php if($model->isNewRecord!='1'){ ?>
				<div class="row">
					<?php echo CHtml::image(Yii::app()->request->baseUrl. '/images/' . $model->image,"image",array("width"=>200));?>
				</div>
				<?php }?>
		</div>
	
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'genre',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->textField($model,'genre',array('size'=>40,'maxlength'=>60, 'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'genre'); ?>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'description',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->textArea($model,'description',array('size'=>60,'maxlength'=>1000, 'class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'description'); ?>
			</div>
		</div>
	
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'price',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->numberField($model,'price',array('class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'price'); ?>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'owner_id',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->dropDownList($model,'owner_id', CHtml::listData(Owner::model()->findAll(), 'owner_id', 'owner_name'),array('class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'owner_id'); ?>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<?php echo $form->labelEx($model,'use',array('class'=> 'col-sm-2 control-label')); ?>
				<div class="col-sm-10">
					<?php echo $form->numberField($model,'use',array('class'=>'form-control')); ?>
				</div>
				<?php echo $form->error($model,'use'); ?>
			</div>
		</div>
	
		<div class="row buttons">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		</div>

<?php $this->endWidget(); ?>

</div><!-- form -->