<?php
	/* Display all books */
?>

<table class="table">
	<tr>
		<th>ID</th>
		<th>User</th>
		<th>Total</th>
		<th>Payment</th>
		<th>Comment</th>
		<th>Date</th>
		<th>Action</th>
	</tr>
	<?php if(isset($models) && $models !== NULL) : ?>
		<?php foreach ($models as $model) : ?>
			<tr>
				<td><?php echo $model->transaction_id;?></td>
				<td><?php echo $model->user_id;?></td>
				<td><?php echo $model->total;?></td>
				<td><?php echo $model->payment;?></td>
				<td><?php echo $model->comment;?></td>
				<td><?php echo $model->insert_date;?></td>
				<td>
					<a href="<?php echo $this-> createUrl ('transaction/update', array('id'=>$model->owner_id));?>"><i class="fa fa-edit fa-lg"></i></a>
					<a href="<?php echo $this-> createUrl ('transaction/del', array('id'=>$model->owner_id));?>" onclick="return confirm('Are you sure you wish to delete this Record?');"><i class="fa fa-times fa-lg"></i></a>
				</td>
			</tr>
		<?php endforeach;?>
	<?php endif;?>
</table>